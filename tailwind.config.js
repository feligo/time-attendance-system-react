/** @type {import('tailwindcss').Config} */
module.exports = {
  content:['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false,
  corePlugins: {
    preflight: false // 禁用 @tailwind base 消除与antd 的css 冲突
  },
  theme: {
    extend: {},
  },
  plugins: [],
}

