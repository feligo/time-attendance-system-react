import type { ReactNode } from 'react';
declare module 'react-router-dom' {
   interface IndexRouteObject {
    meta?: {
      title?: string;
      menu?: boolean;
      auth?: string;
      icon?:any;
      [key: string]: any;
    };
  }
  interface NonIndexRouteObject {
    meta?: {
      title?: string;
      menu?: boolean;
      auth?: string;
      icon?:any;
      [key: string]: any;
    };
  }
}