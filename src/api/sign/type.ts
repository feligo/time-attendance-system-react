import { Response } from 'src/api/types'
export interface RequestSignsTime {
    userid: string
}

export interface TypeSignsInfos {
    // 打卡信息ID
    _id: string,
    // 用户ID
    userid: string,
    // 打卡时间列表
    time: unknown,
    // 出勤情况列表
    detail: unknown
}
export type ResponseSignsTime = Response & {
    infos: TypeSignsInfos
}
