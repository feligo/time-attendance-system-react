import http from '@/utils/axios/http'
import { RequestSignsTime,ResponseSignsTime } from "./type";

// 更新用户打卡信息

export const signsTime = (params:RequestSignsTime) => http.get<RequestSignsTime,ResponseSignsTime>('/signs/time',params)
export const signsUpdateTime = (params:RequestSignsTime) => http.put<RequestSignsTime,ResponseSignsTime>('/signs/time',params)
