import http from '@/utils/axios/http'
import type { RequestApplyList, ResponseApplyList, RequestApplyData } from './type'

//  获取用户审批信息 - 调用此接口可获取用户审批信息详情。
export const applyList = ( params: RequestApplyList ) => http.get<RequestApplyList, ResponseApplyList>( '/checks/apply', params )
export const applyData = ( params: RequestApplyData ) => http.post<RequestApplyData, any>( '/checks/apply', params )
