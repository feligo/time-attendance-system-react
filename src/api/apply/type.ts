import type { Response } from '@/api/types'


export interface RequestApplyList {
    applicantid?: string // 查询条件异常考情：用户ID
    approverid?: string // 查询我的审批：申请人ID
}

export interface ResponseApplyListItem {
    // 审批信息ID
    _id: string
    // 申请人ID
    applicantid: string
    // 申请人姓名
    applicantname: string
    // 审批人ID
    approverid: string
    // 审批人姓名
    approvername: string
    // 审批备注
    note: string
    // 审批事由
    reason: string
    // 审批状态
    state: string
    // 审批时间
    time: string[]
}

export interface ResponseApplyList extends Response {
    rets: ResponseApplyListItem[]
}



export interface RequestApplyData {
    // 申请人ID
    applicantid: string
    // 申请人姓名
    applicantname: string
    // 审批人ID
    approverid: string
    // 审批人姓名
    approvername: string
    // 审批备注
    note: string
    // 审批事由
    reason: string
    // 审批时间
    // time: [DateModelType,DateModelType];
    time: [string,string]
}
export type ApplyFromData = Pick<RequestApplyData, 'approvername'|'note'|'reason'|'time'>
