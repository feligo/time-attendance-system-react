export interface RequestApplyPut {
    _id: string
    state: '已通过' | '未通过'
}
