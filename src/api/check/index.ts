import http from '@/utils/axios/http'
import type { RequestApplyPut } from './type'
import type { Response } from '@/api/types'

//  更新用户审批信息-调用此接口可更新用户审批信息详情。
export const applyPut = ( params: RequestApplyPut ) => http.put<RequestApplyPut, Response>( '/checks/apply', params )