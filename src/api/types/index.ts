export interface Response {
    errcode: number,
    errmsg: string,
    [ key: string ]: unknown
}
