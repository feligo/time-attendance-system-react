import type { Response } from '@/api/types'

export interface RequestRemindAcquire {
    userid: string
}
export interface ResponseRemindAcquire extends Response {
    info:{
        // 消息提醒ID
        _id: string
        // 用户ID
        userid: string
        // 申请消息提醒
        applicant: boolean
        // 审批消息提醒
        approver: boolean
    }
}

export interface RequestRemindUpdate {
    // 用户ID
    userid: string
    // 申请消息提醒
    applicant?: boolean
    // 审批消息提醒
    approver?: boolean
}

export type ResponseRemindUpdate = ResponseRemindAcquire
