import http from '@/utils/axios/http'
import type { RequestRemindAcquire, ResponseRemindAcquire, RequestRemindUpdate, ResponseRemindUpdate } from './type'

//  获取用户审批信息 - 调用此接口可获取用户审批信息详情。
export const remindAcquire = (params: RequestRemindAcquire) => http.get<RequestRemindAcquire, ResponseRemindAcquire>('/news/remind', params)
// 更新消息提醒信息-
export const remindUpdate = (params: RequestRemindUpdate) => http.put<RequestRemindUpdate, ResponseRemindUpdate>('/news/remind', params)
