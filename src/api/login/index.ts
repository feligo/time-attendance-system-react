import http from '@/utils/axios/http'
import type { TypeLoginParams,TypeUserToken,TypeUserInfo} from './type'

//  用户登陆
export const login = (params:TypeLoginParams) => http.post<TypeLoginParams,TypeUserToken>('/users/login',params)

//  获取用户信息 ｜ 登陆后验证
export const info = () => http.get<any,TypeUserInfo>('/users/infos')
