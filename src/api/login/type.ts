import {Response} from 'src/api/types'

//  登陆请求体
export interface TypeLoginParams {
    email: string
    pass: string
}

// 登陆响应体
export interface TypeUserToken extends Response{
    token : string,
}

// 审批人列表数据
export interface TypeApprover {
    _id: string,
    name: string
}
export interface TypeInfos {
    // 用户ID
    _id: string,
    // 用户姓名
    name: string,
    //用户头像
    head: string,
    // 权限路由列表
    permission: string[],
    // 审批人列表
    approver: TypeApprover[]
}
//  用户信息响应体
export interface TypeUserInfo extends Response{
    infos : TypeInfos,
}
