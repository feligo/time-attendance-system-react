import React,{Suspense} from 'react'
import ReactDOM from 'react-dom/client'
import '@/assets/styles/tailwind.css'
import '@/assets/styles/normalize.css'
import '@/assets/styles/iconfont.css'
import '@/assets/styles/antd-reset.scss'
import reportWebVitals from './reportWebVitals'

import { RouterProvider } from 'react-router-dom'
import router from '@/router'

import { Provider } from 'react-redux'
import store from '@/store'

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
)

root.render(
    <React.StrictMode>
        <Suspense>
            <Provider store={store}>
                <RouterProvider router={router}/>
            </Provider>
        </Suspense>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
