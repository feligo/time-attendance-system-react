import { routes } from '@/router'
import { useLocation, matchRoutes } from 'react-router-dom'

export const useRouterInfo = () => {

    const location = useLocation()

    const match = matchRoutes(routes, location)

    return { location, match }
}