export const useDateTimer = () =>{
    // 时间设置
    const date = new Date()
    const dateYear = date.getFullYear()
    const dateMonth = date.getMonth() + 1

    return {date, dateYear, dateMonth}
}