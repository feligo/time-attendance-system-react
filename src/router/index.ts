import React, { lazy } from 'react'

import { createBrowserRouter, Navigate } from 'react-router-dom'
import type { RouteObject } from 'react-router-dom'

import { BeforEach } from '@/middleware'
import DefaultLayout from '@/layouts/DefaultLayout'

import Login from '@/views/login/Login'

import {
    CopyOutlined,
    CalendarOutlined,
    WarningOutlined,
    FileAddOutlined,
    ScheduleOutlined
} from '@ant-design/icons'

export const routes: RouteObject[] = [
    {  // 重定向
        path: '/',
        index: true,
        element: React.createElement(Navigate, { to: '/sign' })
    },
    {
        path: '',
        element: React.createElement(BeforEach, null, React.createElement(DefaultLayout)),
        meta: {
            menu: true,
            title: '考勤管理',
            icon: React.createElement(CopyOutlined),
            auth: 'home'
        },
        children: [
            {
                path: '/sign',
                element: React.createElement(lazy(() => import('@/views/sign'))),
                meta: {
                    menu: true,
                    title: '在线打卡签到',
                    icon: React.createElement(CalendarOutlined),
                    auth: 'sign'
                }
            },
            {
                path: '/exception',
                element: React.createElement(lazy(() => import('@/views/exception'))),
                meta: {
                    menu: true,
                    title: '异常考勤查询',
                    icon: React.createElement(WarningOutlined),
                    auth: 'exception'
                }
            },
            {
                path: '/apply',
                element: React.createElement(lazy(() => import('@/views/apply'))),
                meta: {
                    menu: true,
                    title: '添加考勤审批',
                    icon: React.createElement(FileAddOutlined),
                    auth: 'apply'
                }
            },
            {
                path: '/check',
                element: React.createElement(lazy(() => import('@/views/check'))),
                meta: {
                    menu: true,
                    title: '我的考勤审批',
                    icon: React.createElement(ScheduleOutlined),
                    auth: 'check'
                }
            },

        ]
    },
    {
        path: '/login',
        element: React.createElement(BeforEach,null,React.createElement(Login))
    },
    {
        path: '/403',
        element: React.createElement(lazy(() => import('@/views/NotAuth')))
    },
    {
        path: '/404',
        element: React.createElement(lazy(() => import('@/views/NotFound')))
    },
    {
        path: '/500',
        element: React.createElement(lazy(() => import('@/views/NotServer')))
    },
    {
        path: '*',
        element: React.createElement(Navigate,{to:'/404'})
    }
]

const router = createBrowserRouter(routes)

export default router
