//  月份加零
export const toZero: ( n: number | string ) => string = ( n ) => {
    return n > 10 ? `${ n }` : `0${ n }`
}
