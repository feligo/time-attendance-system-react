import axios from 'axios'
import store from '@/store'
import { clearToken } from '@/store/modules/userStore'
import { message } from 'antd'
import router from '@/router'

const instance = axios.create({
    baseURL: 'http://api.h5ke.top/',
    // baseURL: '/api',
    timeout: 6000
})
// 添加请求拦截器
instance.interceptors.request.use((config) => {
    // 在发送请求之前做些什么
    if (config.headers) {
        config.headers.authorization = store.getState().userStore.token
    }
    return config;
}, (error) => {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use((response) => {
    // 对响应数据做点什么
    const { data } = response
    if (data && data.errcode === 0) {
        return response.data
    } else if (data && data.errmsg === 'token error') {
        message.error('token error')
        store.dispatch(clearToken())
    } else if (data && data.errcode === 500) {
        message.error('server error')
        router.navigate('/500')
    } else {
    // } else if (data && data.errcode === -1) {
        message.error(data.errmsg)
        return response.data
    }
    return response

}, (error) => {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default instance
