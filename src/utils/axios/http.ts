import type {AxiosRequestConfig} from 'axios'
import instance from './index'

// export interface Http {
//     get<T, P>(url: string, params?: p): Promise<T>
// }
// class Axios implements Http {
//     get<T=any,P=any>(url:string, params) {
//         console.log ( dataloj: url, data[lj: params);return Promise.resolve ( value: params);
//
//     }
//
interface Http {
    get<T,U>(url: string,data?: T,config?: AxiosRequestConfig):Promise<U>,
    post<T,U>(url: string,data?: T,config?: AxiosRequestConfig): Promise<U>,
    put<T,U>(url: string,data?: T,config?: AxiosRequestConfig):Promise<U>,
    patch<T,U>(url: string,data?: T,config?: AxiosRequestConfig):Promise<U>,
    delete<T,U>(url: string,data?: T,config?: AxiosRequestConfig):Promise<U>
}

const http: Http = {
    get (url,data,config) {
        return instance.get(url,{params: data,...config})
    },
    post (url,data,config) {
        return instance.post(url,data,config)
    },
    put (url,data,config) {
        return instance.put(url,data,config)
    },
    patch (url,data,config) {
        return instance.patch(url,data,config)
    },
    delete (url,data,config) {
        return instance.delete(url,{data,...config})
    }
}

export default http
