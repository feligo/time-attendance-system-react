import React from 'react'
import { Button, Divider, Input, Radio, Row, Space, Table } from 'antd'
// @ts-ignore
import { useSearch, useTable } from './hook.tsx'

const Check = () => {
    // 搜索
    const { approverStateList, keyword, keywordChange, selectedState, defaultState, approverStateChange } = useSearch()
    // 表格
    const { applyList, columns } = useTable( keyword, defaultState, selectedState, )
    return (
        <>
            <Row className="p-2" justify="end">
                <Space>
                    <Input value={ keyword } onChange={ keywordChange } placeholder="请输入关键词"></Input>
                    <Button>搜索</Button>
                    <Divider style={ { borderLeftColor: '#dcdfe6' } } type="vertical"/>
                    <Radio.Group
                        options={ approverStateList }
                        onChange={ approverStateChange }
                        value={ selectedState }
                        optionType="button"
                        buttonStyle="solid"
                    />
                </Space>
            </Row>
            <Table
                rowKey="_id"
                dataSource={ applyList }
                columns={ columns }
            />
        </>
    )
}
export default Check
