export enum DescriptionsKey {
    normal = '正常出勤',
    absent = '旷工',
    miss = '漏打卡',
    late = '迟到',
    early = '早退',
    lateAndEarly = '迟到并早退'
}