import React from 'react'
import { Descriptions, Calendar, Button, Tag, Row, Space, Select } from 'antd'

import 'dayjs/locale/zh-cn'
import locale from 'antd/es/date-picker/locale/zh_CN'
import { useCalendar } from './hook'
import { DescriptionsKey } from './enums'

const Sign = () => {
    const { detailState, descriptionsData, monthOptions, month, handleCalendarSelect, dateCellRender, handlePutTimeBtb } = useCalendar()
    return (
        <>
            <Descriptions bordered column={9} layout="vertical">
                <Descriptions.Item label="月份">{month}月</Descriptions.Item>
                {
                    //  这种 Object.entries(descriptionsData) 类型会丢失 要重新设置一次 [string,number][]
                    // (Object.entries(descriptionsData) as [string,number][]).map(([ k, val ])=> {
                    //     return (
                    //         <Descriptions.Item label={(DescriptionsKey)[k]} key={val}>{val}</Descriptions.Item>
                    //     )
                    // })
                    Object.entries(DescriptionsKey).map(([ keys, value ]) => {
                        return (<Descriptions.Item label={value} key={keys}>{descriptionsData[keys]}</Descriptions.Item>)
                    })
                }
                <Descriptions.Item label="操作">
                    <Button type="primary" ghost size="small" href={`/exception?month=${month}`}>查看详情</Button>
                </Descriptions.Item>
                <Descriptions.Item label="考勤状态">
                    <Tag color={detailState.type}>{detailState.text}</Tag>
                </Descriptions.Item>
            </Descriptions>
            <Calendar cellRender={dateCellRender} className="mt-5" locale={locale}
                      headerRender={({ value, type, onChange, onTypeChange }) => {
                          return (
                              <Row align="middle" justify="space-between">
                                  <Button type="primary" onClick={handlePutTimeBtb}>在线签到</Button>
                                  <Space>
                                      <Button>{value.year()}年</Button>
                                      <Select value={month} options={monthOptions}
                                              onChange={(val) => handleCalendarSelect(val, value, onChange)}></Select>
                                  </Space>
                              </Row>
                          )
                      }}/>
        </>
    )
}
export default Sign
