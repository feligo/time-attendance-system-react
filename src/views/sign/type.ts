import { DescriptionsKey } from './enums'
export interface TypeDetailState {
    type: 'success' | 'error';
    text: '正常' | '异常';
}

export type TypeDescriptionsData = Record<keyof typeof DescriptionsKey, number>