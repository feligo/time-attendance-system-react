import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'antd'

const NotAuth = () => {
  return (
    <div className="status-wrapper">
      <img src={require('@/assets/images/403.png')} alt="" />
      <p>没有权限</p>
      <Link to="/">
        <Button type="primary">回到首页</Button>
      </Link>
    </div>
  )
}

export default NotAuth
