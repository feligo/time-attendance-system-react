import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'antd'

const NotFound = () => {
  return (
    <div className="status-wrapper">
      <img src={require('@/assets/images/404.png')} alt="" />
      <p>没有找到</p>
      <Link to="/">
        <Button type="primary">回到首页</Button>
      </Link>
    </div>
  )
}
export default NotFound
