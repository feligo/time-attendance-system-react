import React from 'react'
import { Button, Row, Space, Input, Divider, Radio, Table, Modal, Form, Select, DatePicker } from 'antd'
import locale from 'antd/es/date-picker/locale/zh_CN';

import { useApply, useModal } from './hook'

const Apply = () => {

    const {
        approverStateList,
        keyword,
        keywordChange,
        selectedState,
        approverStateChange,
        handelSearchBtn,
        applyList,
        columns
    } = useApply()

    const { isModal, handleIsModal, form, rules, approverList, onFinish } = useModal()
    return (
        <>
            <Row className="p-2" justify="space-between">
                <Button type="primary" onClick={ handleIsModal }>添加评审</Button>
                <Space>
                    <Input value={ keyword } onChange={ keywordChange } placeholder="请输入关键词"></Input>
                    <Button onClick={handelSearchBtn}>搜索</Button>
                    <Divider style={ { borderLeftColor: '#dcdfe6' } } type="vertical"/>
                    <Radio.Group
                        options={ approverStateList }
                        onChange={ approverStateChange }
                        value={ selectedState }
                        optionType="button"
                        buttonStyle="solid"
                    />
                    {/*<Radio.Button value="a">Hangzhou</Radio.Button>*/ }
                </Space>
            </Row>
            <Table
                rowKey="_id"
                dataSource={ applyList }
                columns={ columns }
            />
            <Modal title="添加审批"
                   open={ isModal }
                   onCancel={ handleIsModal }
                   footer={ null }
            >
                <Form
                    form={ form }
                    labelCol={ { span: 4 } }
                    wrapperCol={ { span: 20 } }
                    onFinish={ onFinish }
                    autoComplete="off"
                >
                    <Form.Item name="approvername" label="审批人" rules={ rules.approvername }>
                        <Select placeholder="请选择审批人" allowClear>
                            {
                                Array.isArray( approverList ) && approverList.map( item => (
                                    <Select.Option key={ item._id } value={ item.name }>{ item.name }</Select.Option> ) )
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item name="reason" label="审批事由" rules={ [ { required: true } ] }>
                        <Select placeholder="请选择审批事由" allowClear>
                            <Select.Option value="年假">年假</Select.Option>
                            <Select.Option value="事假">事假</Select.Option>
                            <Select.Option value="病假">病假</Select.Option>
                            <Select.Option value="外出">外出</Select.Option>
                            <Select.Option value="补签卡">补签卡</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item name="time" label="时间" rules={ [ { required: true } ] }>
                        <DatePicker.RangePicker showTime locale={ locale }/>
                    </Form.Item>
                    <Form.Item name="note" label="备注" rules={ [ { required: true } ] }>
                        <Input.TextArea rows={ 4 } placeholder="请输入备注"/>
                    </Form.Item>
                    <Row justify="end">
                        <Space>
                            <Button onClick={ handleIsModal }>取消</Button>
                            <Button type="primary" htmlType="submit">提交</Button>
                        </Space>
                    </Row>
                </Form>
            </Modal>
        </>
    )
}
export default Apply
