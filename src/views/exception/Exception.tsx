import React from 'react'
import { Button, Card, Row, Col, Select, Space, Timeline, Empty } from 'antd'
import useException from './hook'
import style from './style.module.scss'
const Exception = () => {

    const { dateYear, month, monthOptions, currentMonthData, renderTime,handleCalendarSelect,applyListMonth } = useException()

    return (
        <>
            <Row className="p-2" align="middle" justify="space-between">
                <Button type="primary" href={ '/apply' }>异常处理</Button>
                <Space>
                    <Button>{ dateYear }年</Button>
                    <Select value={ `${ month }月` } options={ monthOptions }
                            onChange={ ( val ) => handleCalendarSelect( val ) }></Select>
                </Space>
            </Row>
            <Row className="pt-8 px-2" gutter={ 20 }>
                <Col span={ 12 }>
                    {
                        currentMonthData
                            ? <Timeline>
                                {
                                    currentMonthData.map( item => (
                                        <Timeline.Item key={ item[ 0 ] }>
                                            <h3>{ dateYear }/{ month }/{ item[ 0 ] }</h3>
                                            <Card className={style['punch-info-card']}>
                                                <Space>
                                                    <h4>{ item[ 1 ] }</h4>
                                                    <p>考勤详情：{ renderTime( item[ 0 ] ) }</p>
                                                </Space>
                                            </Card>
                                        </Timeline.Item>
                                    ) )
                                }
                            </Timeline>
                            : <Empty/>
                    }
                </Col>
                <Col span={ 12 }>
                    {
                        applyListMonth.length
                        ? <Timeline>
                                {
                                    applyListMonth.map( item => (
                                        <Timeline.Item key={item._id as string}>
                                            <h3>{item.reason as string}</h3>
                                            <Card className={style['exception-card']}>
                                                <h4>{item.state as string}</h4>
                                                <p className={style['exception-content']}>申请日期 { (item.time as string[])[0] } - { (item.time as string[])[1] }</p>
                                                <p className={style['exception-content']}>申请详情 { item.note as string }</p>
                                            </Card>
                                        </Timeline.Item>
                                    ))
                                }
                            </Timeline>
                            : <Empty/>
                    }


                </Col>
            </Row>
        </>
    )
}
export default Exception
