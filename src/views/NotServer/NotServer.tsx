import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'antd'

const NotServer = () => {
  return (
    <div className="status-wrapper">
      <img src={require('@/assets/images/500.png')} alt="" />
      <p>服务异常</p>
      <Link to="/">
        <Button type="primary">回到首页</Button>
      </Link>
    </div>
  )
}

export default NotServer
