import React from 'react'
import { useNavigate } from 'react-router-dom'

import { useAppDispatch } from '@/store'
import { loginAction, updateToken } from '@/store/modules/userStore'

import classnames from 'classnames'
import styles from './styles.module.scss'

import { Button, Form, Input, Row, Col, message } from 'antd'

import type { TypeLoginParams, TypeUserToken } from '@/api/login/type'


const testUsers: TypeLoginParams[] = [
    {
        email: 'huangrong@imooc.com',
        pass: 'huangrong'
    },
    {
        email: 'hongqigong@imooc.com',
        pass: 'hongqigong'
    }
]

const Login = () => {

    const navigate = useNavigate()
    const dispatch = useAppDispatch()

    const [ form ] = Form.useForm()
    const [ messageApi, contextHolder ] = message.useMessage();
    const onFinish = async (values: TypeLoginParams) => {
        const { payload } = await dispatch(loginAction(values))
        const { errcode, token } = payload as TypeUserToken
        if (errcode !== 0) return false
        dispatch(updateToken(token as string))
        messageApi.open({
            type: 'success',
            content: '登录成功'
        });
        navigate('/')
    };

    const onFinishFailed = ({ values }: any) => {
        console.log('Failed:', values);
    }

    const autoLogin = (values: TypeLoginParams) => {
        return () => {
            form.setFieldsValue(values)
            onFinish(values)
        }
    }

    return (
        <>
            {contextHolder}
            <div className={styles.login}>
                <div className={styles.header}>
                <span className={styles['header-logo']}>
                  <i className={classnames('iconfont icon-react', styles['icon-react'])}></i>
                  <i className={classnames('iconfont icon-icon-test', styles['icon-icon-test'])}></i>
                  <i className={classnames('iconfont icon-typescript', styles['icon-typescript'])}></i>
                </span>
                    <span className={styles['header-title']}>在线考勤系统</span>
                </div>
                <div className={styles.desc}>
                    零基础从入门到进阶，系统掌握前端三大热门技术(Vue、React、TypeScript)
                </div>
                <Form
                    name="basic"
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 18 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    className={styles.main}
                    form={form}
                >
                    <Form.Item<TypeLoginParams>
                        label="邮箱"
                        name="email"
                        rules={[
                            { required: true, message: '请输入邮箱!' },
                            { type: 'email', message: '请输入正确的邮箱地址!' }
                        ]}
                    >
                        <Input placeholder="请输入邮箱"/>
                    </Form.Item>

                    <Form.Item<TypeLoginParams>
                        label="密码"
                        name="pass"
                        rules={[ { required: true, message: '请输入密码!' } ]}
                    >
                        <Input.Password visibilityToggle={false} placeholder="请输入密码"/>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                        <Button type="primary" htmlType="submit">
                            登录
                        </Button>
                    </Form.Item>
                </Form>
                <div className={styles.users}>
                    <Row>
                        {
                            testUsers.map(item => (
                                <Col key={item.pass}>
                                    <h3>
                                        测试账号，<Button
                                        onClick={autoLogin({ email: item.email, pass: item.pass })}>一键登录</Button>
                                    </h3>
                                    <p>邮箱：{item.email}</p>
                                    <p>密码：{item.pass}</p>
                                </Col>
                            ))
                        }
                    </Row>
                </div>
            </div>
        </>
    )
}
export default Login