import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import to from 'await-to-js'
import type { PayloadAction } from '@reduxjs/toolkit'
import { signsTime } from '@/api/sign'
import type { TypeSignsInfos } from '@/api/sign/type'

const signStore = createSlice( {
    name: 'signStore',
    initialState: {
        signData: {} as TypeSignsInfos
    },
    reducers: { // 更新数据
        updataSignData( state, action:PayloadAction<TypeSignsInfos> ) {
            state.signData = action.payload
        }
    }
} )

export const { updataSignData } = signStore.actions
export const signAction = createAsyncThunk(
    'sign/signAction',
    async (params:string) => {
        const [ error, res ] = await to( signsTime( { userid: params} ) )
        error && console.error( error )
        return res
    }
)

export default signStore.reducer;
