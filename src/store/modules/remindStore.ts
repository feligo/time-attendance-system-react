import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import to from 'await-to-js'
import type { PayloadAction } from '@reduxjs/toolkit'
import { remindAcquire, remindUpdate } from '@/api/remind'
import type { RequestRemindAcquire, ResponseRemindAcquire, RequestRemindUpdate } from '@/api/remind/type'


const remindStore = createSlice({
    name: 'remindStore',
    initialState: {
        remindInfo: {} as ResponseRemindAcquire['info']
    },
    reducers: { // 更新数据
        updataRemindData(state, action: PayloadAction<ResponseRemindAcquire['info']>) {
            state.remindInfo = action.payload
        }
    }
})

export const { updataRemindData } = remindStore.actions

//  请求站内信息表
export const remindAction = createAsyncThunk(
    'remind/remindAction',
    async (params: RequestRemindAcquire) => {
        const [ error, res ] = await to(remindAcquire(params))
        error && console.error(error)
        return res
    }
)

//  请求站内信息-更新
export const remindAcquireUpdate = createAsyncThunk(
    'remind/remindAcquireUpdate',
    async (params: RequestRemindUpdate) => {
        const [ error, res ] = await to(remindUpdate(params))
        error && console.error(error)
        return res
    }
)

export default remindStore.reducer
