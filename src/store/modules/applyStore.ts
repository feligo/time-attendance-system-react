import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import to from 'await-to-js'
import type { PayloadAction } from '@reduxjs/toolkit'
import { applyList } from '@/api/apply'
import type { ResponseApplyListItem,RequestApplyList } from "@/api/apply/type";


const applyStore = createSlice( {
    name: 'applyStore',
    initialState: {
        applyList: [] as ResponseApplyListItem[]
    },
    reducers: { // 更新数据
        updataApplyData( state, action:PayloadAction<ResponseApplyListItem[]> ) {
            state.applyList = action.payload
        }
    }
} )

// 获取用户审批信息
export const { updataApplyData } = applyStore.actions
export const applyAction = createAsyncThunk(
    'apply/applyAction',
    async (params:RequestApplyList) => {
        const [ error, res ] = await to( applyList( params) )
        error && console.error( error )
        return res
    }
)

export default applyStore.reducer;
