import React from 'react'
import styles from './styles.module.scss'
import { Menu } from 'antd'
// @ts-ignore
import { useMenuData } from './hook.tsx'

const onClick = () => {

}
const DefaultLayoutSider = () => {

    const { currentPath, currentSubPath, menuList } = useMenuData()

    return (
        <Menu
            onClick={onClick}
            selectedKeys={[ currentSubPath ]}
            openKeys={[ currentPath ]}
            mode="inline"
            items={menuList}
            className={styles['default-layout-sider']}
        />
    )
}
export default DefaultLayoutSider