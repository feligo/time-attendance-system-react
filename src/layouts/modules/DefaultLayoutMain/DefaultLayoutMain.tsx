import React from 'react'
import { Outlet } from 'react-router-dom'
const DefaultLayoutMain = () => {
    return (
        <Outlet></Outlet>
    )
}
export default DefaultLayoutMain