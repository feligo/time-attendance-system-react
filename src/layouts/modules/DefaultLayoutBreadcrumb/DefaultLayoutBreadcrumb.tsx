import React from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumb } from 'antd'

import { useRouterInfo } from '@/hooks/routetHook'

const DefaultLayoutBreadcrumb = () => {

    const { match } = useRouterInfo()

    const breadcrumbList = match!.map(item => {
        return {
            title: (<Link to={item.pathnameBase}>{item.route.meta?.title}</Link>)
        }
    })
    return (
        <Breadcrumb
            className='bg-white px-8 py-2.5'
            items={breadcrumbList}
        />
    )
}
export default DefaultLayoutBreadcrumb