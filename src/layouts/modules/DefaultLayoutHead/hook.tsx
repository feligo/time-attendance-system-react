import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import { useSelector } from 'react-redux'
import { useAppDispatch } from '@/store'
import { remindAction, updataRemindData } from '@/store/modules/remindStore'

import { clearToken } from '@/store/modules/userStore'
import type { MenuProps } from 'antd'
import type { RootState } from '@/store'
import type { TypeInfos } from '@/api/login/type'
import type { ResponseRemindAcquire } from '@/api/remind/type'


export const useHeadData = () => {

    const dispatch = useAppDispatch()

    const remindInfo = useSelector((state: RootState) => state.remindStore.remindInfo)

    const userId = useSelector((state: RootState) => state.userStore.infos._id)
    const getRemindAcquire = async () => {
        const { payload } = await dispatch(remindAction({ userid: userId }))
        const { info } = payload as ResponseRemindAcquire
        dispatch(updataRemindData(info))
    }

    useEffect(() => {
        if (_.isEmpty(remindInfo)) {
            getRemindAcquire()
        }
    }, [ userId, remindInfo ])

    const isDot = (remindInfo.applicant || remindInfo.approver)

    const msgReminderList: MenuProps['items'] = [
        // {
        //     key: '1',
        //     label: <Link to="/apply">有审批结果消息</Link>
        // },
        // {
        //     key: '2',
        //     label: <Link to="/check">有审批请求消息</Link>
        // },
        // {
        //     key: '3',
        //     label: <div>暂无消息</div>
        // }
    ]

    // if ( remindInfo.applicant ) {
    //     msgReminderList.push( {
    //         key: '1',
    //         label: <Link to="/apply">有审批结果消息</Link>
    //     } )
    // } else if ( remindInfo.approver ) {
    //     msgReminderList.push( {
    //         key: '2',
    //         label: <Link to="/check">有审批请求消息</Link>
    //     } )
    // } else {
    //     msgReminderList.push( {
    //         key: '3',
    //         label: <div>暂无消息</div>
    //     } )
    // }

    if ( remindInfo.applicant ) {
        msgReminderList.push( {
            key: '1',
            label: <Link to="/apply">有审批结果消息</Link>
        } )
    }
    if ( remindInfo.approver ) {
        msgReminderList.push( {
            key: '2',
            label: <Link to="/check">有审批请求消息</Link>
        } )
    }
    if(!remindInfo.applicant && !remindInfo.approver){
        msgReminderList.push( {
            key: '3',
            label: <div>暂无消息</div>
        } )
    }

    // 用户登出
    const handleLogOut = () => {
        dispatch(clearToken())
        window.location.replace('/login')
    }

    const personalMenuList: MenuProps['items'] = [
        {
            key: '1',
            label: (<p>个人菜单</p>)
        },
        {
            key: '2',
            label: (<p onClick={handleLogOut}>退出</p>)
        }
    ]

    const { name: userName, head: userHead } = useSelector((state: RootState) => state.userStore.infos) as TypeInfos


    return { msgReminderList, personalMenuList, userName, userHead, isDot }
}
