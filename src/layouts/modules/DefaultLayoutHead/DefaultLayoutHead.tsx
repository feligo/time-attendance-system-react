import React from 'react'
import { Dropdown, Avatar, Badge } from 'antd'
import { BellOutlined } from '@ant-design/icons'

import { useHeadData } from './hook'

const DefaultLayoutHead = () => {

    const { msgReminderList, personalMenuList,userName,userHead,isDot } = useHeadData()

    return (
        <div className="h-full flex justify-between items-center">
            <h2>考勤系统</h2>
            <section className="flex items-center">
                <Dropdown menu={{ items: msgReminderList }} placement="bottom" arrow>
                    <Badge dot={isDot}>
                        <BellOutlined style={{ fontSize: 20 }}/>
                    </Badge>
                </Dropdown>
                <Dropdown className="mx-4" menu={{ items: personalMenuList }} placement="bottom" arrow>
                    <Avatar src={<img src={userHead} alt="avatar"/>}/>
                </Dropdown>
                { userName }
            </section>
        </div>
    )
}
export default DefaultLayoutHead
