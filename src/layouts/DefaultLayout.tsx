import React from 'react'
import { DefaultLayoutHead, DefaultLayoutSider, DefaultLayoutMain, DefaultLayoutBreadcrumb } from './modules'

import { Layout } from 'antd'

const { Header, Sider, Content } = Layout;
const DefaultLayout = () => {
    return (

        <Layout>
            <Header className="bg-white">
                <DefaultLayoutHead/>
            </Header>
            <Layout hasSider>
                <Sider className="reset-sider" theme="light" width={300}>
                    <DefaultLayoutSider></DefaultLayoutSider>
                </Sider>
                <Layout className="m-5">
                    <DefaultLayoutBreadcrumb/>
                    <Content className="bg-white mt-5 p-2.5">
                        <DefaultLayoutMain/>
                    </Content>
                </Layout>

            </Layout>
        </Layout>
    )
}
export default DefaultLayout